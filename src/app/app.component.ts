import { Component, OnInit } from '@angular/core';
import { EmployeeModel } from './employee';
import { EmployeeinfoService } from './employeeinfo.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'firstapp';

  formValue !: FormGroup;
  // showAdd!: true;
  // showUpdate!: false;
  employeemodelobj: EmployeeModel = new EmployeeModel();
  employeeData !: any;

  constructor(private serviceref: EmployeeinfoService, private frmbldr: FormBuilder) {
    this.formValue = this.frmbldr.group({
      firstName: [' ',[Validators.required]],
      lastName: [' '],
      email: [' '],
      phone: [' ']
    })
  }
  // get f(){
  //   return this.formValue.controls;
  // }
  

  ngOnInit(): void {
    this.getAllEmployee();
  }
  // clickAddEmployee(){
  //   this.formValue.reset;
  //   this.showAdd=true;
  //   this.showUpdate=false;
  // }

  postEmployeeDetails() {
    this.employeemodelobj.firstName = this.formValue.value.firstName;
    this.employeemodelobj.lastName = this.formValue.value.lastName;
    this.employeemodelobj.email = this.formValue.value.email;
    this.employeemodelobj.phone = this.formValue.value.phone;

    this.serviceref.postEmployee(this.employeemodelobj).subscribe(
      res => {
        alert("Employee Added Succefully");
        this.formValue.reset();
        this.getAllEmployee();
      },
      err => {
        alert("Somthing Went Wrong");
      }
    )
  }


  getAllEmployee() {
    this.serviceref.getEmployee().subscribe(
      res => {
        this.employeeData = res;
      })
  }


  deleteEmployeeid(row: any) {
    this.serviceref.deleteEmployee(row.id)
      .subscribe(res => {
        alert("Employee Deleted");
        this.getAllEmployee();
      })
  }


  onEdit(row: any) {

    // this.showAdd=false;
    // this.showUpdate=true;

    this.employeemodelobj.id = row.id;
    this.formValue.controls['firstName'].setValue(row.firstName);
    this.formValue.controls['lastName'].setValue(row.lastName);
    this.formValue.controls['email'].setValue(row.email);
    this.formValue.controls['phone'].setValue(row.phone);
  }


  updateEmployeeDetails() {
    this.employeemodelobj.firstName = this.formValue.value.firstName;
    this.employeemodelobj.lastName = this.formValue.value.lastName;
    this.employeemodelobj.email = this.formValue.value.email;
    this.employeemodelobj.phone = this.formValue.value.phone;

    this.serviceref.updateEmployee(this.employeemodelobj, this.employeemodelobj.id)
      .subscribe(res => {
        alert("Updated Succefully");
        this.formValue.reset;
        this.getAllEmployee();
      })
  }
}
