import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/Operators'
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class EmployeeinfoService {

  constructor(private httpref: HttpClient) { }

  postEmployee(data: any) {
    return this.httpref.post<any>('http://localhost:3000/Employee', data)
      .pipe(map((res: any) => {
        return res;
      }))
  }
  getEmployee():Observable<any> {
    return this.httpref.get<any>('http://localhost:3000/Employee')
      .pipe(map((res: any) => {
        return res;
      }))
  }
  updateEmployee(data:any,id:number) {
    return this.httpref.put<any>(`${'http://localhost:3000/Employee'}/${id}`,data)
      .pipe(map((res: any) => {
        return res;
      }))
  }
  deleteEmployee(id:number):Observable<any> {
    return this.httpref.delete<any>(`${'http://localhost:3000/Employee'}/${id}`)
      .pipe(map((res: any) => {
        return res;
      }))
  }

}
